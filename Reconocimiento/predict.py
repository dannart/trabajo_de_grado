from django.http import HttpResponse
from django.template import context, Template
import numpy as np
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model
from django.shortcuts import render


longitud, altura = 150, 150
modelo = '/code/Reconocimiento/modelo/modelo.h5'
pesos_modelo = '/code/Reconocimiento/modelo/pesos.h5'
cnn = load_model(modelo)
cnn.load_weights(pesos_modelo) 

def predict(file):
  x = load_img(file, target_size=(longitud, altura)) 
  x = img_to_array(x) 
  x = np.expand_dims(x, axis=0) 
  array = cnn.predict(x) 
  result = array[0] 
  answer = np.argmax(result)
  
  if answer == 0:
    rta = "Predicción: Antracnosis. Las hojas tiernas son generalmente las más afectadas. Las manchas de apariencia de mojada en el follaje o los frutos son los primeros indicios visibles. El tejido de las hojas muere, adquiriendo una textura de papel y un color marrón a medida que la enfermedad se propaga."
  elif answer == 1:
    rta = "Predicción: Pasador. Las orugas más jóvenes cavan diminutos túneles en el interior de las hojas dejando a su paso estelas serpenteantes de color blanco o gris. Al crecer, salen de sus túneles y permanecen en la superficie de las hojas royendo partes de las epidermis inferior o superior. Esto produce en las hojas formaciones que parecen ventanas de color marrón claro que a veces se secan y caen, dejando un orificio irregular."
  elif answer == 2:
    rta = "Predicción: Saludable"
  
  return rta


